﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows; 
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using WinForms = System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Drawing;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;
using static System.Windows.Forms.DataFormats;

namespace Assignment2
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private FilterInfoCollection videoDevices;
		private VideoCaptureDevice videoSource;

		public MainWindow()
		{
			InitializeComponent();
			Loaded += MainWindow_Loaded;
			Closed += MainWindow_Closed;
		}

		private void btn_browse_Click(object sender, RoutedEventArgs e)
		{
			WinForms.FolderBrowserDialog dialog = new WinForms.FolderBrowserDialog();
			WinForms.DialogResult result = dialog.ShowDialog();
			if (result == WinForms.DialogResult.OK)
			{
				txtFileName.Text = dialog.SelectedPath;
				//string path = dialog.SelectedPath;
				//string[] files = Directory.GetFiles(path);

				List<FileSystemItem> fileSystemItemList = GetChildren(dialog.SelectedPath);
				listView.ItemsSource = fileSystemItemList;


				//DirectoryInfo directoryInfo = new DirectoryInfo(dialog.SelectedPath);
				//FileSystemInfo[] items = directoryInfo.GetFileSystemInfos();

				//List<FileSystemItem> fileSystemItemList = new List<FileSystemItem>();

				//foreach (FileSystemInfo item in items)
				//{
				//	FileSystemItem fileSystemItem = new FileSystemItem
				//	{
				//		Type = (item is DirectoryInfo) ? "📂" : "📄",
				//		//Type = (item is DirectoryInfo) ? BitmapFrame.Create(new Uri("pack://application:,,,/YourAppName;component/Resources/icon1.ico")) : null,
				//		Name = item.Name,
				//		Path = item.FullName
				//	};

				//	fileSystemItemList.Add(fileSystemItem);
				//}

				//listView.ItemsSource = fileSystemItemList;
			}

		}

		private List<FileSystemItem> GetChildren(string folderPath)
		{
			List<FileSystemItem> children = new List<FileSystemItem>();

			try
			{
				string[] directories = Directory.GetDirectories(folderPath);
				foreach (string directory in directories)
				{
					FileSystemItem item = new FileSystemItem
					{
						Type = "📂",
						Name = System.IO.Path.GetFileName(directory),
						Path = directory
					};

					children.Add(item);
				}

				string[] files = Directory.GetFiles(folderPath);
				foreach (string file in files)
				{
					FileSystemItem item = new FileSystemItem
					{
						//Type = "📄",
						Name = System.IO.Path.GetFileName(file),
						Path = file
					};

					string extension = System.IO.Path.GetExtension(file).ToLower();
					if (extension == ".png" || extension == ".jpg" || extension == ".jpeg")
					{
						item.Type = "🖼";
					}
					else
					{
						item.Type = "📄";
					}

					children.Add(item);
				}
			}
			catch (Exception ex)
			{
				// Xử lý các ngoại lệ nếu cần thiết
			}

			return children;
		}


		//private void listView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
		//{
		//	ListView listView = (ListView)sender;


		//	if (listView.SelectedItem != null)
		//	{
		//		// Clicked on a file or folder
		//		listView.ContextMenu = new ContextMenu();

		//		MenuItem deleteMenuItem = new MenuItem();
		//		deleteMenuItem.Header = "Delete";
		//		listView.ContextMenu.Items.Add(deleteMenuItem);

		//		MenuItem renameMenuItem = new MenuItem();
		//		renameMenuItem.Header = "Rename";
		//		listView.ContextMenu.Items.Add(renameMenuItem);
		//	}

		//	listView.ContextMenu.PlacementTarget = listView;
		//	listView.ContextMenu.IsOpen = true;
		//	e.Handled = true;
		//}


		private void listView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (listView.SelectedItem != null)
			{
				FileSystemItem selected = (FileSystemItem)listView.SelectedItem;
				if (selected.Type == "📂")
				{
					txtFileName.Text = selected.Path;
					List<FileSystemItem> children = GetChildren(selected.Path);
					listView.ItemsSource = children;
				}
				else
				{
					try
					{
						System.Diagnostics.Process.Start("explorer.exe", selected.Path);
					}
					catch (Exception ex)
					{
						Console.WriteLine("Lỗi mở file: " + ex.Message);
						// Xử lý lỗi mở file
					}
				}
			}
		}

		private void listView_ContextMenuOpening(object sender, ContextMenuEventArgs e)
		{
			var listView = (ListView)sender;
			listView.ContextMenu = new ContextMenu();

			var clickedItem = FindClickedItem(e);

			if (clickedItem != null)
			{
				MenuItem deleteMenuItem = new MenuItem();
				deleteMenuItem.Header = "Delete File";
				deleteMenuItem.Click += DeleteMenuItem_Click;
				listView.ContextMenu.Items.Add(deleteMenuItem);

				MenuItem renameMenuItem = new MenuItem();
				renameMenuItem.Header = "Rename File";
				renameMenuItem.Click += RenameMenuItem_Click;
				listView.ContextMenu.Items.Add(renameMenuItem);
			}
			else
			{
				// Clicked on empty space
				MenuItem createFolderMenuItem = new MenuItem();
				createFolderMenuItem.Header = "Create Folder";
				createFolderMenuItem.Click += CreateFolderMenuItem_Click;
				listView.ContextMenu.Items.Add(createFolderMenuItem);

				MenuItem createFileMenuItem = new MenuItem();
				createFileMenuItem.Header = "Create File";
				createFileMenuItem.Click += CreateFileMenuItem_Click;
				listView.ContextMenu.Items.Add(createFileMenuItem);

				//MenuItem deleteMenuItem = new MenuItem();
				//deleteMenuItem.Header = "Delete File";
				//listView.ContextMenu.Items.Add(deleteMenuItem);

				//MenuItem renameMenuItem = new MenuItem();
				//renameMenuItem.Header = "Rename File";
				//listView.ContextMenu.Items.Add(renameMenuItem);
			}
		}

		private ListViewItem FindClickedItem(ContextMenuEventArgs e)
		{
			var listView = (ListView)e.Source;
			var clickedItem = (ListViewItem)listView.ContainerFromElement(e.OriginalSource as DependencyObject);

			return clickedItem;
		}
		public string GetParentPath(string path)
		{
			string parentPath = System.IO.Path.GetDirectoryName(path);
			return parentPath;
		}

		private void CreateFolderMenuItem_Click(object sender, RoutedEventArgs e)
		{
			// Hiển thị hộp thoại nhập tên thư mục mới
			string folderName = Microsoft.VisualBasic.Interaction.InputBox("Enter folder name:", "Create Folder", txtFileName.Text + "\\");

			if (!string.IsNullOrEmpty(folderName))
			{
				try
				{
					string selectedFolderPath = txtFileName.Text; // Lấy đường dẫn thư mục được chọn
					string newFolderPath = System.IO.Path.Combine(selectedFolderPath, folderName);

					// Kiểm tra xem thư mục đã tồn tại chưa
					if (Directory.Exists(newFolderPath) || File.Exists(newFolderPath))
					{
						MessageBox.Show("Folder already exists.");
					}
					else
					{
						// Tạo thư mục mới
						Directory.CreateDirectory(newFolderPath);

						// Cập nhật danh sách hiển thị
						RefreshListView(selectedFolderPath);
					}
				}
				catch (Exception ex)
				{
					// Xử lý lỗi khi tạo thư mục
					MessageBox.Show("Error creating folder: " + ex.Message);
				}
			}
		}

		private void CreateFileMenuItem_Click(object sender, RoutedEventArgs e)
		{
			// Hiển thị hộp thoại nhập tên thư mục mới
			string folderName = Microsoft.VisualBasic.Interaction.InputBox("Enter folder name:", "Create Folder", txtFileName.Text + "\\");

			if (!string.IsNullOrEmpty(folderName))
			{
				try
				{
					string selectedFolderPath = txtFileName.Text; // Lấy đường dẫn thư mục được chọn
					string newFolderPath = System.IO.Path.Combine(selectedFolderPath, folderName);

					// Kiểm tra xem thư mục đã tồn tại chưa
					if (File.Exists(newFolderPath))
					{
						MessageBox.Show("Folder already exists.");
					}
					else
					{

						// Tạo tệp tin mới
						using (File.Create(newFolderPath))
						{
							// Cập nhật danh sách hiển thị
							RefreshListView(selectedFolderPath);
						}
					}
				}
				catch (Exception ex)
				{
					// Xử lý lỗi khi tạo thư mục
					MessageBox.Show("Error creating folder: " + ex.Message);
				}
			}
		}

		private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
		{
			if (listView.SelectedItem != null)
			{
				FileSystemItem selectedItem = (FileSystemItem)listView.SelectedItem;

				string parentPath = GetParentPath(selectedItem.Path);

				// Kiểm tra xem mục được chọn là tệp tin hay thư mục
				if (selectedItem.Type == "📂") // Thư mục
				{
					try
					{
						// Xóa thư mục
						Directory.Delete(selectedItem.Path, true);

						// Cập nhật danh sách hiển thị
						RefreshListView(parentPath);
					}
					catch (Exception ex)
					{
						// Xử lý lỗi khi xóa thư mục
						MessageBox.Show("Error deleting folder: " + ex.Message);
					}
				}
				else
				{
					try
					{
						// Xóa tệp tin
						File.Delete(selectedItem.Path);

						// Cập nhật danh sách hiển thị
						RefreshListView(parentPath);
					}
					catch (Exception ex)
					{
						// Xử lý lỗi khi xóa tệp tin
						MessageBox.Show("Error deleting file: " + ex.Message);
					}
				}
			}
		}

		private void RenameMenuItem_Click(object sender, RoutedEventArgs e)
		{
			if (listView.SelectedItem != null)
			{
				FileSystemItem selectedItem = (FileSystemItem)listView.SelectedItem;

				// Hiển thị hộp thoại nhập tên mới
				string newName = Microsoft.VisualBasic.Interaction.InputBox("Enter new name:", "Rename", selectedItem.Name);

				if (!string.IsNullOrEmpty(newName))
				{
					try
					{
						string parentPath = System.IO.Path.GetDirectoryName(selectedItem.Path);
						string newPath = System.IO.Path.Combine(parentPath, newName);

						if (selectedItem.Type == "📂") // Thư mục
						{
							Directory.Move(selectedItem.Path, newPath);
						}
						else if (selectedItem.Type == "📄") // Tệp tin
						{
							File.Move(selectedItem.Path, newPath);
						}

						// Cập nhật danh sách hiển thị
						RefreshListView(parentPath);
					}
					catch (Exception ex)
					{
						// Xử lý lỗi khi đổi tên
						MessageBox.Show("Error renaming item: " + ex.Message);
					}
				}
			}

		}

		private void RefreshListView(string path)
		{
			// Lấy danh sách mục con của thư mục cha
			List<FileSystemItem> children = GetChildren(path);

			// Cập nhật danh sách hiển thị
			listView.ItemsSource = children;
		}


		// Back to previous path
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			string parentPath = GetParentPath(txtFileName.Text);
			txtFileName.Text = parentPath;
			RefreshListView(parentPath);
		}


		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
		}
		private void MainWindow_Closed(object sender, EventArgs e)
		{
			if (videoSource != null && videoSource.IsRunning)
			{
				videoSource.SignalToStop();
				videoSource.WaitForStop();
			}
		}

		private void VideoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
		{
			img_cam.Dispatcher.Invoke(() =>
			{
				// Convert AForge VideoFrame to Bitmap
				Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
				img_cam.Source = BitmapToImageSource(bitmap);
			});
		}

		private BitmapImage BitmapToImageSource(Bitmap bitmap)
		{
			using (MemoryStream memoryStream = new MemoryStream())
			{
				bitmap.Save(memoryStream, ImageFormat.Bmp);
				memoryStream.Position = 0;

				BitmapImage bitmapImage = new BitmapImage();
				bitmapImage.BeginInit();
				bitmapImage.StreamSource = memoryStream;
				bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
				bitmapImage.EndInit();

				return bitmapImage;
			}
		}


		private void btnCap_Click(object sender, RoutedEventArgs e)
		{
			if (videoSource != null && videoSource.IsRunning)
			{
				BitmapSource bitmapSource = (BitmapSource)img_cam.Source;
				string parentPath = txtFileName.Text;
				if (bitmapSource != null)
				{
					// Convert BitmapSource to Bitmap
					Bitmap bitmap = BitmapFromSource(bitmapSource);
					string desiredPath = txtFileName.Text;
					// Save the captured image
					string fileName = $"CapturedImage_{DateTime.Now:yyyyMMddHHmmss}.jpg";
					string filePath = System.IO.Path.Combine(desiredPath, fileName);

					bitmap.Save(filePath, ImageFormat.Jpeg);

					RefreshListView(parentPath);


					MessageBox.Show($"Image captured and saved at:\n{filePath}", "Capture Successful", MessageBoxButton.OK, MessageBoxImage.Information);
				}
				else
				{
					MessageBox.Show("No image to capture.", "Capture Failed", MessageBoxButton.OK, MessageBoxImage.Warning);
				}

				
			}
			else
			{
				MessageBox.Show("Webcam is not running.", "Capture Failed", MessageBoxButton.OK, MessageBoxImage.Warning);
			}
		}

		private Bitmap BitmapFromSource(BitmapSource bitmapsource)
		{
			Bitmap bitmap;
			using (MemoryStream outStream = new MemoryStream())
			{
				BitmapEncoder enc = new PngBitmapEncoder();
				enc.Frames.Add(BitmapFrame.Create(bitmapsource));
				enc.Save(outStream);
				bitmap = new Bitmap(outStream);
			}
			return bitmap;
		}

		private void btnOpenCam_Click(object sender, RoutedEventArgs e)
		{
			if (videoSource == null && videoDevices.Count > 0)
			{
				videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString);
				videoSource.NewFrame += VideoSource_NewFrame;
				videoSource.Start();
				((Button)sender).Content = "Stop Webcam";
			}
			else if (videoSource != null)
			{
				videoSource.SignalToStop();
				videoSource.WaitForStop();
				videoSource = null;
				((Button)sender).Content = "Start Webcam";
			}
		}





		//private void SaveImageToFile(Bitmap capturedBitmap, string filePath, ImageFormat format, long quality)
		//{
		//	try
		//	{
		//		// Thiết lập thông số cho việc lưu ảnh
		//		EncoderParameters encoderParameters = new EncoderParameters(1);
		//		encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

		//		// Lấy thông tin về định dạng ảnh cần lưu
		//		ImageCodecInfo imageCodecInfo = GetEncoderInfo(format);

		//		// Lưu ảnh vào tệp tin
		//		capturedBitmap.Save(filePath, imageCodecInfo, encoderParameters);
		//	}
		//	catch (Exception ex)
		//	{
		//		MessageBox.Show("Lỗi: " + ex.Message);
		//	}
		//}

		//private ImageCodecInfo GetEncoderInfo(ImageFormat format)
		//{
		//	ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

		//	foreach (ImageCodecInfo codec in codecs)
		//	{
		//		if (codec.FormatID == format.Guid)
		//		{
		//			return codec;
		//		}
		//	}

		//	return null;
		//}

	}



	public class FileSystemItem
	{
		public string Type { get; set; }
		public string Name { get; set; }
		public string Path { get; set; }
	}
}
