﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Package : MonoBehaviour
{
	private Color color; // Mảng chứa màu sắc tương ứng với từng loại package
	public float rotateSpeed = 50f; // Tốc độ quay của package

	private void Start()
	{
		color = GetComponent<SpriteRenderer>().color; // Gán màu sắc cho sprite của package
	}

	private void Update()
	{
		// Quay package
		transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
	}

	public void SetPackageColor(Color packageColor)
	{
		color = packageColor;
		GetComponent<SpriteRenderer>().color = color; // Gán màu sắc mới cho sprite của package
	}

	public Color GetPackageColor()
	{
		return color;
	} 
}
