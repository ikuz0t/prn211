using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DurabilityBar : MonoBehaviour
{
	[SerializeField] GameObject durability;

	public void SetDurability(float durabilityNormalized)
	{
		durability.transform.localScale = new Vector3(durabilityNormalized, 1f, 1f);
	}

	public void SetDecreaseDurabilitySmooth(float newDurability)
	{
		float curDurability = durability.transform.localScale.x;
		float changeAmt = curDurability - newDurability;

		while (curDurability - newDurability > Mathf.Epsilon)
		{
			curDurability -= changeAmt * Time.deltaTime;
			durability.transform.localScale = new Vector3(curDurability, 1f);
			//yield return null;
		}
		durability.transform.localScale = new Vector3(newDurability, 1f);
	}

	public void SetIncreaseDurabilitySmooth(float newDurability)
	{
		float curDurability = durability.transform.localScale.x;
		float changeAmt = newDurability - curDurability;

		while (newDurability - curDurability > Mathf.Epsilon)
		{
			curDurability += changeAmt * Time.deltaTime;
			durability.transform.localScale = new Vector3(curDurability, 1f);
			//yield return null;
		}
		durability.transform.localScale = new Vector3(newDurability, 1f);
	}
}
