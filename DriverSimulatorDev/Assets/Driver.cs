﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    [SerializeField] private Sprite[] carSprites;
	private int currentSpriteIndex = 0; // Chỉ số của sprite hiện tại
	private SpriteRenderer spriteRenderer; // Tham chiếu đến component SpriteRenderer của car

	[SerializeField] private float steerspeed = 250f;
    [SerializeField] private float movespeed = 20f;
    [SerializeField] private float boostSpeed = 30f;
    [SerializeField] private float slowSpeed = 15f;

	[SerializeField] private Color32 noPackageColor = new Color32(1, 1, 1, 1);
	//[SerializeField] private Color32 hasPackageColor = new Color32(1, 1, 1, 1);
	[SerializeField] private float PickupDelay = 0.5f;

	[SerializeField] StatusHud statusHud;

	private int countPackage = 0;
	private bool hasPackage;
	private bool canChangeColor = false; // Cờ cho phép thay đổi loại xe
	private bool canRepair = false; // Cờ cho phép thay đổi loại xe

	public int CountCompletePackage { get; set; } = 0;

    public float CurrentDurability { get; set; }

	public Driver()
	{
		CurrentDurability = 100;
	}

	private void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>(); // Lấy tham chiếu đến SpriteRenderer
		spriteRenderer.sprite = carSprites[currentSpriteIndex]; // Gán sprite mặc định cho car
	}


	// Update is called once per frame
	private void Update()
    {
		if (!statusHud.Fulltime() && CurrentDurability > 0)
		{
			// Code di chuyển
			float steerAmount = Input.GetAxis("Horizontal") * steerspeed * Time.deltaTime;
			float moveAmount = Input.GetAxis("Vertical") * movespeed * Time.deltaTime;
			transform.Rotate(0, 0, -steerAmount);
			transform.Translate(0, moveAmount, 0);
		}      
        else
		{
			statusHud.OpenEndScreen();
		}


		if (!statusHud.Fulltime() && CountCompletePackage == GameController.Instance.maxPackages)
        {
            statusHud.OpenCompleteScreen();
        }


        if (Input.GetKeyDown(KeyCode.F) && canChangeColor)
		{
			// Khi nhấn phím "F", tăng chỉ số sprite lên 1 (hoặc chuyển vòng lại sprite đầu nếu đã ở sprite cuối cùng)
			currentSpriteIndex = (currentSpriteIndex + 1) % carSprites.Length;
			spriteRenderer.sprite = carSprites[currentSpriteIndex]; // Gán sprite mới cho car
		}

		if (Input.GetKey(KeyCode.F) && canRepair) // nhấn giữ GetKey
		{
			if (CurrentDurability < 100)
			{
				CurrentDurability += 0.01f;
				statusHud.UpdateCurrentDurabilityIfIncrease();
			}
		}
	}

    private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.tag == "Boost")
		{
			Debug.Log("Boost");
			movespeed = boostSpeed;
		}
		else if (collision.tag == "House")
		{
			canChangeColor = true;
		}
		else if (collision.tag == "Package"/* && !hasPackage*/)
		{
			Debug.Log("Get Package!");
			countPackage++;
			//hasPackage = true;
			//spriteRenderer.color = collision.gameObject.GetComponent<Package>().GetPackageColor();
			//Destroy(collider.gameObject, PickupDelay);
			GameController.Instance.DestroyGameObject(collision.gameObject, PickupDelay);

			statusHud.TakePackage();
		}
		else if (collision.tag == "Customer" && countPackage > 0)
		{
            //if(ColorApproximately(spriteRenderer.color, collision.GetComponent<Customer>().GetCustomerColor()))
            //{
            //	Debug.Log("Done!");
            //	hasPackage = false;
            //	spriteRenderer.color = noPackageColor;

            //	statusHud.AddScore();
            //}
            Debug.Log("Done!");
			countPackage--;
            CountCompletePackage++;

            GameController.Instance.DestroyGameObject(collision.gameObject, PickupDelay);

            statusHud.AddScore();
			statusHud.DecreasePackage();
        }
		else if (collision.tag == "Repair")
		{
			canRepair = true;
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "House")
		{
			canChangeColor = false;
		}
		else if (collision.tag == "Repair")
		{
			canRepair = false;
		}
	}

	private void OnCollisionEnter2D(Collision2D collision) {
		Debug.Log("Collision");
		movespeed = slowSpeed;

		CurrentDurability -= 2;
		statusHud.UpdateCurrentDurabilityIfDecrease();
    }

	private bool ColorApproximately(Color color1, Color color2, float tolerance = 0.01f)
	{
		float rDiff = Mathf.Abs(color1.r - color2.r);
		float gDiff = Mathf.Abs(color1.g - color2.g);
		float bDiff = Mathf.Abs(color1.b - color2.b);
		float aDiff = Mathf.Abs(color1.a - color2.a);

		return rDiff <= tolerance && gDiff <= tolerance && bDiff <= tolerance && aDiff <= tolerance;
	}
}
