﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class GameController : MonoBehaviour
{
    [SerializeField] private Color[] colors;
    [SerializeField] private GameObject packagePrefab;
    [SerializeField] private GameObject customerPrefab;
    [SerializeField] private Transform packagesParent; // Game object chứa các package
    [SerializeField] private Transform customersParent; // Game object chứa các customer
    [SerializeField] private Transform[] spawnPointsPackage;
    [SerializeField] private Transform[] spawnPointsCustomer;
    [SerializeField] private int maxCustomers = 5;

    public int maxPackages = 5;


    private int currentPackages = 0;
    private int currentCustomers = 0;
    private List<Transform> usedSpawnPoints = new List<Transform>();

    public static GameController Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        SpawnObjects(packagePrefab, packagesParent, spawnPointsPackage, maxPackages, ref currentPackages);
        SpawnObjects(customerPrefab, customersParent, spawnPointsCustomer, maxCustomers, ref currentCustomers);
    }

    private void SpawnObjects(GameObject prefab, Transform parent, Transform[] spawnPoints, int maxObjects, ref int currentObjects)
    {
        for (int i = 0; i < maxObjects; i++)
        {
            if (currentObjects >= maxObjects)
                break;

            int randomIndex = GetRandomUnusedSpawnPointIndex(spawnPoints);
            Transform spawnPoint = spawnPoints[randomIndex];

            GameObject newObject = Instantiate(prefab, spawnPoint.position, Quaternion.identity);
            newObject.transform.SetParent(parent); // Đặt parent cho đối tượng mới

            currentObjects++;
            usedSpawnPoints.Add(spawnPoint);
        }
    }

    public void DestroyGameObject(GameObject gameObject, float pickupDelay)
    {
        Destroy(gameObject, pickupDelay);
    }

    private int GetRandomUnusedSpawnPointIndex(Transform[] spawnPoints)
    {
        while (true)
        {
            int randomIndex = Random.Range(0, spawnPoints.Length);
            if (!usedSpawnPoints.Contains(spawnPoints[randomIndex]))
            {
                return randomIndex;
            }
        }
    }
}




//public class GameController : MonoBehaviour
//{
//	[SerializeField] private Color[] colors;
//    [SerializeField] private GameObject packagePrefab; // Prefab của package
//    [SerializeField] private GameObject customerPrefab; // Prefab của package

//    [SerializeField] private Transform[] spawnPointsPackage; // Mảng chứa các vị trí xuất hiện package
//	[SerializeField] private int maxPackages = 5; // Số lượng package tối đa

//    [SerializeField] private Transform[] spawnPointsCustomer; // Mảng chứa các vị trí xuất hiện Customer
//    [SerializeField] private int maxCustomers = 5; // Số lượng Customer tối đa

//    private int currentPackages = 0; // Số lượng package hiện tại
//    private int currentCustomers = 0; // Số lượng package hiện tại


//	private List<Transform> usedSpawnPoints = new List<Transform>(); // Danh sách các vị trí đã sử dụng

//	public static GameController Instance { get; set; }

//	private void Awake()
//	{
//		Instance = this;
//	}

//	private void Start()
//	{
//		SpawnPackages();
//		SpawnCustomers();
//	}

//	private void SpawnPackages()
//	{
//		for (int i = 0; i < maxPackages; i++)
//		{
//			SpawnPackage();
//			//SpawnCustomer();
//		}
//	}

//    private void SpawnCustomers()
//    {
//        for (int i = 0; i < maxCustomers; i++)
//        {
//            SpawnCustomer();
//        }
//    }


//    private void SpawnPackage()
//	{
//		if (currentPackages >= maxPackages) return; // Kiểm tra nếu đã đạt đến số lượng package tối đa

//		//int randomIndex = Random.Range(0, spawnPointsPackage.Length); // Chọn ngẫu nhiên một vị trí xuất hiện trong mảng spawnPoints
//		int randomPackageIndex = GetRandomUnusedSpawnPointIndex(spawnPointsPackage); // Chọn ngẫu nhiên một vị trí xuất hiện trong mảng spawnPoints

//		Transform spawnPoint = spawnPointsPackage[randomPackageIndex];

//		GameObject newPackage = Instantiate(packagePrefab, spawnPoint.position, Quaternion.identity); // Sinh ra package tại vị trí spawnPoint
//		Package packageScript = newPackage.GetComponent<Package>(); // Lấy reference tới script Package được gắn trên package mới

//		Color randomColor = GetRandomColor(colors); // Lấy màu ngẫu nhiên từ mảng colors
//		packageScript.SetPackageColor(randomColor); // Gán màu sắc cho package mới

//		currentPackages++;
//		usedSpawnPoints.Add(spawnPoint); // Thêm vị trí đã sử dụng vào danh sách
//	}

//	private void SpawnCustomer()
//	{
//        if (currentCustomers >= maxCustomers) return;

//        int randomCustomerIndex = GetRandomUnusedSpawnPointCustomerIndex(spawnPointsCustomer);
//        Transform spawnPoint = spawnPointsCustomer[randomCustomerIndex];
//        Instantiate(customerPrefab, spawnPoint.position, Quaternion.identity);

//		currentCustomers++;
//		usedSpawnPoints.Add(spawnPoint);

//    }


//    public void DestroyGameObject(GameObject gameObject, float pickupDelay)
//	{
//		Destroy(gameObject, pickupDelay); // Phá hủy package sau khoảng tg ...
//		//currentPackages--;
//		//if (currentPackages < maxPackages) SpawnPackage(); // Tạo ra một package mới
//	}

//	private Color GetRandomColor(Color[] colors)
//	{
//		int randomIndex = Random.Range(0, colors.Length);
//		return colors[randomIndex];
//	}

//	private int GetRandomUnusedSpawnPointIndex(Transform[] spawnPoints)
//	{
//		while (true)
//		{
//			int randomIndex = Random.Range(0, spawnPoints.Length);
//			if (!usedSpawnPoints.Contains(spawnPointsPackage[randomIndex]))
//			{
//				return randomIndex;
//			}
//		}
//	}
//	private int GetRandomUnusedSpawnPointCustomerIndex(Transform[] spawnPoints)
//	{
//		while (true)
//		{
//			int randomIndex = Random.Range(0, spawnPoints.Length);
//			if (!usedSpawnPoints.Contains(spawnPointsCustomer[randomIndex]))
//			{
//				return randomIndex;
//			}
//		}
//	}
//}
