﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class StatusHud : MonoBehaviour
{
	[SerializeField] private float countdownTime = 90f; // Thời gian đếm ngược ban đầu
	[SerializeField] private Text timerText;
	[SerializeField] private Text pointText;
	[SerializeField] private Text pointTakeText;
	[SerializeField] private GameObject newGame;
	[SerializeField] private GameObject complete;
	[SerializeField] private Text durabilityText;
	[SerializeField] private DurabilityBar durabilityBar;
	[SerializeField] private Driver driver;
	[SerializeField] private int numberScene = -1;
	

    public Camera firstCamera;
	public Camera secondCamera;

	private bool isSecondCameraActive = false; // Trạng thái hiện tại của camera

	private int score;
	private int takePackage;

	private void Start()
	{
		durabilityText.text = driver.CurrentDurability + "/100";
		durabilityBar.SetDurability((float)driver.CurrentDurability / 100);

		firstCamera.enabled = !isSecondCameraActive; // Kích hoạt camera thứ nhất khi trạng thái là false
		secondCamera.enabled = isSecondCameraActive; // Kích hoạt camera thứ hai khi trạng thái là true
	}

	private void Update()
	{
		if (countdownTime > 0)
		{
			countdownTime -= Time.deltaTime; // Giảm thời gian đếm ngược theo thời gian đã trôi qua

			if (countdownTime <= 0)
			{
				countdownTime = 0; // Đảm bảo thời gian không nhỏ hơn 0
			}

			timerText.text = "Time: " + Mathf.FloorToInt(countdownTime).ToString("#,#") + "s"; // Hiển thị thời gian đếm ngược trên giao diện người dùng
		}

		if (Input.GetKeyDown(KeyCode.G))
		{
			isSecondCameraActive = !isSecondCameraActive; // Đảo ngược trạng thái của camera

			firstCamera.enabled = !isSecondCameraActive; // Kích hoạt/ vô hiệu hóa camera thứ nhất tùy thuộc vào trạng thái
			secondCamera.enabled = isSecondCameraActive; // Kích hoạt/ vô hiệu hóa camera thứ hai tùy thuộc vào trạng thái
		}
	}

	public void AddScore()
	{
		score += 1;
		//countdownTime += 20;
		pointText.text = "Số package đã giao: " + score.ToString("#,#") +"/7";
	}

    public void TakePackage()
    {
		takePackage += 1;
		pointTakeText.text = "Số package đang giữ: " + takePackage.ToString("#,#");
    }

    public void DecreasePackage()
    {
        takePackage -= 1;
		if (takePackage > 0) pointTakeText.text = "Số package đang giữ: " + takePackage.ToString("#,#");
        else pointTakeText.text = "Số package đang giữ: " + takePackage.ToString("0");
    }

    public bool Fulltime()
	{
		if (countdownTime <= 0) return true;
		return false;
	}


	public void UpdateCurrentDurabilityIfIncrease()
	{
		durabilityText.text = (int)driver.CurrentDurability + "/100";
		durabilityBar.SetIncreaseDurabilitySmooth((float)driver.CurrentDurability / 100);
	}

	public void UpdateCurrentDurabilityIfDecrease()
	{
		durabilityText.text = (int)driver.CurrentDurability + "/100";
		//Debug.Log(driver.CurrentDurability);
		durabilityBar.SetDecreaseDurabilitySmooth((float)driver.CurrentDurability / 100);
	}

	public void OpenEndScreen()
	{
		Time.timeScale = 0;
		newGame.SetActive(true);
	}

    public void OpenCompleteScreen()
    {
		Time.timeScale = 0;
		complete.SetActive(true);
    }


    public void RestartGame()
	{
		countdownTime = 200f;
		score = 0;
		driver.CurrentDurability = 100;
		driver.CountCompletePackage = 0;
		Time.timeScale = 1;
		SceneManager.LoadScene(numberScene);
	}

	public void NextStage()
	{
        countdownTime = 200f;
        score = 0;
        driver.CurrentDurability = 100;
        driver.CountCompletePackage = 0;
        Time.timeScale = 1;
        SceneManager.LoadScene(numberScene + 1);
	}
}
