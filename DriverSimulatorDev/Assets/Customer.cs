﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour
{
	[SerializeField] private Color color;

	private void Start()
	{
		color = GetComponent<SpriteRenderer>().color; // Gán màu sắc cho sprite của customer
	}

	public Color GetCustomerColor()
	{
		return color;
	}

}
