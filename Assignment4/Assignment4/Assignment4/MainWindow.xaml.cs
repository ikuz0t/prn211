﻿using Assignment4.Models;
using CsvHelper;
using ExcelDataReader;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Formats.Asn1;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WinForms = System.Windows.Forms;

namespace Assignment4
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{

		Assignment4Context db = new Assignment4Context();

		public MainWindow()
		{
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			string filePath = txtFileName.Text;

			if (!filePath.IsNullOrEmpty())
			{
				var students = ReadFromCsv(filePath);

				var topAverageMathStudents = students
					.GroupBy(s => s.Province)
					.Select(g => new
					{
						Province = g.Key,
						AverageMathematics = g.Select(s =>
						{
							double math;
							if (double.TryParse(s.Mathematics, out math))
							{
								return math;
							}
							return 0; // hoặc giá trị mặc định khác nếu cần thiết
						}).Average()
					})
					.OrderByDescending(s => s.AverageMathematics)
					.Take(3);

				string message = "Điểm toán trung bình cao nhất của 3 tỉnh thành:\n\n";
				foreach (var student in topAverageMathStudents)
				{
					message += $"Tỉnh {student.Province}: {student.AverageMathematics} điểm\n";
				}

				WinForms.MessageBox.Show(message);
			}
			else
			{
				WinForms.MessageBox.Show("Vui lòng chọn file để thực hiện.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private List<Students> ReadFromCsv(string filePath)
		{
			var students = new List<Students>();
			using (var reader = new StreamReader(filePath))
			{
				reader.ReadLine(); // skip header line
				while (!reader.EndOfStream)
				{
					var line = reader.ReadLine();
					var values = line.Split(',');
					var student = new Students
					{
						StudentId = values[0],
						Province = values[1],
						Mathematics = values[2],
						Literature = values[3],
						Physics = values[4],
						Chemistry = values[5],
						Biology = values[6],
						CombinedNaturalSciences = values[7],
						History = values[8],
						Geography = values[9],
						CivicEducation = values[10],
						CombinedSocialSciences = values[11],
						English = values[12],
					};
					students.Add(student);
				}
			}
			return students;
		}

		private List<StudentScore> ReadFromFileCsv(string filePath)
		{
			var students = new List<StudentScore>();
			using (var reader = new StreamReader(filePath))
			{
				reader.ReadLine(); // Bỏ qua dòng tiêu đề
				while (!reader.EndOfStream)
				{
					var line = reader.ReadLine();
					var values = line.Split(',');

					var student = new StudentScore
					{
						StudentId = values[0],
						Province = values[1],
						Scores = new List<SubjectScore>()
					};

					for (int i = 2; i < values.Length; i++)
					{
						int subjectId = GetSubjectNameByIndex(i); // Hàm để lấy tên môn học tương ứng với chỉ số cột

						if (!string.IsNullOrEmpty(values[i]))
						{
							double scoreValue = double.Parse(values[i]);
							student.Scores.Add(new SubjectScore { SubjectId = subjectId, Score = scoreValue });
						}
					}

					students.Add(student);
				}
			}

			return students;

			//List<StudentScore> students = new List<StudentScore>();

			//using (var reader = new StreamReader(filePath))
			//{
			//	reader.ReadLine(); // Bỏ qua dòng tiêu đề
			//	while (!reader.EndOfStream)
			//	{
			//		var line = reader.ReadLine();
			//		var values = line.Split(',');

			//		var student = new StudentScore
			//		{
			//			StudentId = values[0],
			//			Province = values[1]
			//		};

			//		for (int i = 2; i < values.Length; i++)
			//		{
			//			int subjectId = GetSubjectNameByIndex(i); // Hàm để lấy tên môn học tương ứng với chỉ số cột

			//			if (!string.IsNullOrEmpty(values[i]))
			//			{
			//				double scoreValue = double.Parse(values[i]);
			//				student.Scores[subjectId] = scoreValue;
			//			}
			//		}

			//		students.Add(student);
			//	}
			//}

			//return students;
		}

		private int GetSubjectNameByIndex(int columnIndex)
		{
			switch (columnIndex)
			{
				case 2:
					return 1;
				case 3:
					return 2;
				case 4:
					return 3;
				case 5:
					return 4;
				case 6:
					return 5;
				case 7:
					return 6;
				case 8:
					return 7;
				case 9:
					return 8;
				case 10:
					return 9;
				case 11:
					return 10;
				case 12:
					return 11;
				default:
					throw new ArgumentOutOfRangeException("Invalid column index.");
			}
		}

		private void btnBrown_Click(object sender, RoutedEventArgs e)
		{
			WinForms.OpenFileDialog dialog = new WinForms.OpenFileDialog();
			WinForms.DialogResult result = dialog.ShowDialog();
			if (result == WinForms.DialogResult.OK)
			{
				txtFileName.Text = dialog.FileName;
			}
		}


		

		private void btnImport_Click(object sender, RoutedEventArgs e)
		{
			string filePath = txtFileName.Text;
			int year;
			string selectedValue = comboBox.Text;

			if (int.TryParse(selectedValue, out year) && !string.IsNullOrEmpty(filePath))
			{
				//var students = ReadFromFileCsv(filePath);

				try
				{				
					using (var connection = new SqlConnection("Server=(local);uid=sa;pwd=12345;database=Assignment4;TrustServerCertificate=True;"))
					{
						connection.Open();

						// Kiểm tra xem năm đã tồn tại trong cơ sở dữ liệu hay chưa
						using (var command = new SqlCommand("SELECT COUNT(*) FROM Student WHERE ExamYear = @Year", connection))
						{
							command.Parameters.AddWithValue("@Year", year);
							int count = (int)command.ExecuteScalar();

							if (count == 0)
							{
								var bulkCopy = new SqlBulkCopy(connection)
								{
									DestinationTableName = "Student", // Thay thế "Student" bằng tên bảng của bạn
									BatchSize = 10000, // Số lượng hàng trong mỗi lần sao chép
									BulkCopyTimeout = 180, // Thời gian chờ tối đa cho mỗi lần sao chép (giây)
									//NotifyAfter = 1000, // Số lượng hàng sao chép trước khi gửi sự kiện thông báo
									//EnableStreaming = true, // Cho phép sao chép dữ liệu theo dòng (streaming)								
								};

								string[] lines = File.ReadAllLines(filePath);
								var dataTable = new DataTable();
								dataTable.Columns.Add("Id", typeof(int));
								dataTable.Columns.Add("StudentCode", typeof(string));
								dataTable.Columns.Add("ExamYear", typeof(int));
								dataTable.Columns.Add("Province", typeof(string));
								dataTable.Columns.Add("Mathematics", typeof(double));
								dataTable.Columns.Add("Literature", typeof(double));
								dataTable.Columns.Add("Physics", typeof(double));
								dataTable.Columns.Add("Chemistry", typeof(double));
								dataTable.Columns.Add("Biology", typeof(double));
								dataTable.Columns.Add("CombinedNaturalSciences", typeof(double));
								dataTable.Columns.Add("History", typeof(double));
								dataTable.Columns.Add("Geography", typeof(double));
								dataTable.Columns.Add("CivicEducation", typeof(double));
								dataTable.Columns.Add("CombinedSocialSciences", typeof(double));
								dataTable.Columns.Add("English", typeof(double));

								for (int lineCount = 1; lineCount < lines.Length; lineCount++)
								{
									string line = lines[lineCount];

									// Xử lý dòng dữ liệu và trích xuất thông tin sinh viên
									string[] values = line.Split(',');

									// Thêm dữ liệu vào DataTable
									dataTable.Rows.Add(
										null,
										values[0], // StudentCode
										year, // ExamYear
										values[1], // Province
										string.IsNullOrEmpty(values[2]) ? 0 : double.Parse(values[2]), // Mathematics
										string.IsNullOrEmpty(values[3]) ? 0 : double.Parse(values[3]), // Literature
										string.IsNullOrEmpty(values[4]) ? 0 : double.Parse(values[4]), // Physics
										string.IsNullOrEmpty(values[5]) ? 0 : double.Parse(values[5]), // Chemistry
										string.IsNullOrEmpty(values[6]) ? 0 : double.Parse(values[6]), // Biology
										string.IsNullOrEmpty(values[7]) ? 0 : double.Parse(values[7]), // CombinedNaturalSciences
										string.IsNullOrEmpty(values[8]) ? 0 : double.Parse(values[8]), // History
										string.IsNullOrEmpty(values[9]) ? 0 : double.Parse(values[9]), // Geography
										string.IsNullOrEmpty(values[10]) ? 0 : double.Parse(values[10]), // CivicEducation
										string.IsNullOrEmpty(values[11]) ? 0 : double.Parse(values[11]), // CombinedSocialSciences
										string.IsNullOrEmpty(values[12]) ? 0 : double.Parse(values[12]) // English
									);
								}

								// Insert dữ liệu từ DataTable vào cơ sở dữ liệu sử dụng SqlBulkCopy
								bulkCopy.WriteToServer(dataTable);

								// Đóng kết nối sau khi hoàn thành công việc
								connection.Close();


								//using (var reader = new StreamReader(filePath))
								//{
								//	var dataTable = new DataTable();
								//	dataTable.Columns.Add("Id", typeof(int));
								//	dataTable.Columns.Add("StudentCode", typeof(string));
								//	dataTable.Columns.Add("ExamYear", typeof(int));
								//	dataTable.Columns.Add("Province", typeof(string));
								//	dataTable.Columns.Add("Mathematics", typeof(double));
								//	dataTable.Columns.Add("Literature", typeof(double));
								//	dataTable.Columns.Add("Physics", typeof(double));
								//	dataTable.Columns.Add("Chemistry", typeof(double));
								//	dataTable.Columns.Add("Biology", typeof(double));
								//	dataTable.Columns.Add("CombinedNaturalSciences", typeof(double));
								//	dataTable.Columns.Add("History", typeof(double));
								//	dataTable.Columns.Add("Geography", typeof(double));
								//	dataTable.Columns.Add("CivicEducation", typeof(double));
								//	dataTable.Columns.Add("CombinedSocialSciences", typeof(double));
								//	dataTable.Columns.Add("English", typeof(double));

								//	string line;
								//	int lineCount = 0;

								//	while ((line = reader.ReadLine()) != null)
								//	{
								//		// Bỏ qua dòng đầu
								//		if (lineCount == 0)
								//		{
								//			lineCount++;
								//			continue;
								//		}

								//		lineCount++;
								//		// Xử lý dòng dữ liệu và trích xuất thông tin sinh viên
								//		string[] values = line.Split(',');

								//		// Thêm dữ liệu vào DataTable
								//		dataTable.Rows.Add(
								//			null,
								//			values[0], // StudentCode
								//			year, // ExamYear
								//			values[1], // Province
								//			string.IsNullOrEmpty(values[2]) ? 0 : double.Parse(values[2]), // Mathematics
								//			string.IsNullOrEmpty(values[3]) ? 0 : double.Parse(values[3]), // Literature
								//			string.IsNullOrEmpty(values[4]) ? 0 : double.Parse(values[4]), // Physics
								//			string.IsNullOrEmpty(values[5]) ? 0 : double.Parse(values[5]), // Chemistry
								//			string.IsNullOrEmpty(values[6]) ? 0 : double.Parse(values[6]), // Biology
								//			string.IsNullOrEmpty(values[7]) ? 0 : double.Parse(values[7]), // CombinedNaturalSciences
								//			string.IsNullOrEmpty(values[8]) ? 0 : double.Parse(values[8]), // History
								//			string.IsNullOrEmpty(values[9]) ? 0 : double.Parse(values[9]), // Geography
								//			string.IsNullOrEmpty(values[10]) ? 0 : double.Parse(values[10]), // CivicEducation
								//			string.IsNullOrEmpty(values[11]) ? 0 : double.Parse(values[11]), // CombinedSocialSciences
								//			string.IsNullOrEmpty(values[12]) ? 0 : double.Parse(values[12]) // English

								//		);

								//	}

								//	// Insert dữ liệu từ DataTable vào cơ sở dữ liệu sử dụng SqlBulkCopy
								//	bulkCopy.WriteToServer(dataTable);
								//}
							}
							else
							{
								// Năm đã tồn tại trong cơ sở dữ liệu, thực hiện xử lý tương ứng
								WinForms.MessageBox.Show("Năm học đã được import, vui lòng chọn năm học khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}


						

							//using (var connection = new SqlConnection("Server=(local);uid=sa;pwd=12345;database=Assignment4;TrustServerCertificate=True;"))
							//{
							//	connection.Open();

							//	var createStudentQuery = "INSERT INTO Student (StudentCode, ExamYear, Province, Mathematics, Literature, Physics, Chemistry, Biology, Combined_Natural_Sciences, History, Geography, Civic_Education, Combined_Social_Sciences, English) VALUES (@StudentCode, @ExamYear, @Province, @Mathematics, @Literature, @Physics, @Chemistry, @Biology, @CombinedNaturalSciences, @History, @Geography, @CivicEducation, @CombinedSocialSciences, @English);";

							//	using (var command = new SqlCommand(createStudentQuery, connection))
							//	{
							//		command.Parameters.Add("@StudentCode", SqlDbType.VarChar, 20);
							//		command.Parameters.Add("@ExamYear", SqlDbType.Int);
							//		command.Parameters.Add("@Province", SqlDbType.NVarChar, 100);
							//		command.Parameters.Add("@Mathematics", SqlDbType.Float);
							//		command.Parameters.Add("@Literature", SqlDbType.Float);
							//		command.Parameters.Add("@Physics", SqlDbType.Float);
							//		command.Parameters.Add("@Chemistry", SqlDbType.Float);
							//		command.Parameters.Add("@Biology", SqlDbType.Float);
							//		command.Parameters.Add("@CombinedNaturalSciences", SqlDbType.Float);
							//		command.Parameters.Add("@History", SqlDbType.Float);
							//		command.Parameters.Add("@Geography", SqlDbType.Float);
							//		command.Parameters.Add("@CivicEducation", SqlDbType.Float);
							//		command.Parameters.Add("@CombinedSocialSciences", SqlDbType.Float);
							//		command.Parameters.Add("@English", SqlDbType.Float);

							//		using (var reader = new StreamReader(filePath))
							//		{
							//			Queue<string[]> dataQueue = new Queue<string[]>();

							//			string line;
							//			int lineCount = 0;
							//			while ((line = reader.ReadLine()) != null)
							//			{
							//				// Bỏ qua dòng đầu
							//				if (lineCount == 0)
							//				{
							//					lineCount++;
							//					continue;
							//				}

							//				// Xử lý dòng dữ liệu và trích xuất thông tin sinh viên
							//				string[] values = line.Split(',');

							//				// Thêm dữ liệu vào hàng đợi FIFO
							//				dataQueue.Enqueue(values);

							//				lineCount++;
							//			}

							//			// Thực hiện insert từng dòng dữ liệu theo thứ tự FIFO
							//			while (dataQueue.Count > 0)
							//			{
							//				string[] values = dataQueue.Dequeue();

							//				string studentCode = values[0];
							//				string province = values[1];
							//				double mathematics = string.IsNullOrEmpty(values[2]) ? 0 : double.Parse(values[2]);
							//				double literature = string.IsNullOrEmpty(values[3]) ? 0 : double.Parse(values[3]);
							//				double physics = string.IsNullOrEmpty(values[4]) ? 0 : double.Parse(values[4]);
							//				double chemistry = string.IsNullOrEmpty(values[5]) ? 0 : double.Parse(values[5]);
							//				double biology = string.IsNullOrEmpty(values[6]) ? 0 : double.Parse(values[6]);
							//				double combinedNaturalSciences = string.IsNullOrEmpty(values[7]) ? 0 : double.Parse(values[7]);
							//				double history = string.IsNullOrEmpty(values[8]) ? 0 : double.Parse(values[8]);
							//				double geography = string.IsNullOrEmpty(values[9]) ? 0 : double.Parse(values[9]);
							//				double civicEducation = string.IsNullOrEmpty(values[10]) ? 0 : double.Parse(values[10]);
							//				double combinedSocialSciences = string.IsNullOrEmpty(values[11]) ? 0 : double.Parse(values[11]);
							//				double english = string.IsNullOrEmpty(values[12]) ? 0 : double.Parse(values[12]);

							//				// Gán giá trị vào các tham số
							//				command.Parameters["@StudentCode"].Value = studentCode;
							//				command.Parameters["@ExamYear"].Value = year;
							//				command.Parameters["@Province"].Value = province;
							//				command.Parameters["@Mathematics"].Value = mathematics;
							//				command.Parameters["@Literature"].Value = literature;
							//				command.Parameters["@Physics"].Value = physics;
							//				command.Parameters["@Chemistry"].Value = chemistry;
							//				command.Parameters["@Biology"].Value = biology;
							//				command.Parameters["@CombinedNaturalSciences"].Value = combinedNaturalSciences;
							//				command.Parameters["@History"].Value = history;
							//				command.Parameters["@Geography"].Value = geography;
							//				command.Parameters["@CivicEducation"].Value = civicEducation;
							//				command.Parameters["@CombinedSocialSciences"].Value = combinedSocialSciences;
							//				command.Parameters["@English"].Value = english;

							//				// Thực hiện insert vào cơ sở dữ liệu
							//				command.ExecuteNonQuery();
							//			}
							//		}
							//	}
							//}
						}

				}
				catch (Exception ex)
				{
					// Xử lý ngoại lệ
					throw;
				}
			}
			else if (string.IsNullOrEmpty(filePath))
			{
				WinForms.MessageBox.Show("Vui lòng chọn file để import.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
				WinForms.MessageBox.Show("Vui lòng chọn năm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
		

		private void btnClear_Click(object sender, RoutedEventArgs e)
		{
			int year;
			string selectedValue = comboBox.Text;

			if (int.TryParse(selectedValue, out year))
			{
				var studentsToDelete = db.Students.Where(s => s.ExamYear == year).ToList();

				if (studentsToDelete.Any())
				{
					db.Students.RemoveRange(studentsToDelete);
					db.SaveChanges();

					WinForms.MessageBox.Show("Đã xóa dữ liệu thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					WinForms.MessageBox.Show("Không tìm thấy năm học.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				WinForms.MessageBox.Show("Vui lòng chọn năm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

		}

		private void btnShow_Click(object sender, RoutedEventArgs e)
		{
		//var studentListEachYears = new List<StudentListEachYear>();

			var students = db.Students.ToList();

			var listViewItems = students
				.GroupBy(schoolYear => schoolYear.ExamYear) // Nhóm theo năm thi
				.Select(group => new StudentListEachYear
				{
					Year = group.Key ?? 0, // Năm thi  // toán tử null
					TotalStudents = group.Count(), // Tổng số sinh viên trong năm thi
					Mathematics = group.Count(schoolYear => schoolYear.Mathematics > 0), // Đếm số lượng sinh viên môn Toán
					Literature = group.Count(schoolYear => schoolYear.Literature > 0), // Đếm số lượng sinh viên môn Văn học
					Physics = group.Count(schoolYear => schoolYear.Physics > 0), // Đếm số lượng sinh viên môn Vật lý
					Chemistry = group.Count(schoolYear => schoolYear.Chemistry > 0), // Đếm số lượng sinh viên môn Hóa học
					Biology = group.Count(schoolYear => schoolYear.Biology > 0), // Đếm số lượng sinh viên môn Sinh học
					CombinedNaturalSciences = group.Count(schoolYear => schoolYear.CombinedNaturalSciences > 0), // Đếm số lượng sinh viên môn Khoa học tự nhiên tổ hợp
					History = group.Count(schoolYear => schoolYear.History > 0), // Đếm số lượng sinh viên môn Lịch sử
					Geography = group.Count(schoolYear => schoolYear.Geography > 0), // Đếm số lượng sinh viên môn Địa lý
					CivicEducation = group.Count(schoolYear => schoolYear.CivicEducation > 0), // Đếm số lượng sinh viên môn Giáo dục công dân
					CombinedSocialSciences = group.Count(schoolYear => schoolYear.CombinedSocialSciences > 0), // Đếm số lượng sinh viên môn Khoa học xã hội tổ hợp
					English = group.Count(schoolYear => schoolYear.English > 0) // Đếm số lượng sinh viên môn Tiếng Anh
				})
				.ToList();

			listView.ItemsSource = listViewItems;


			//var students = db.Students.ToList();

			//var listViewItems = students
			//	.GroupBy(schoolYear => schoolYear.ExamYear) // Nhóm theo năm thi
			//	.Select(group => new StudentListEachYear
			//	{
			//		Year = group.Key ?? 0, // Năm thi
			//		TotalStudents = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id)), // Tổng số sinh viên trong năm thi
			//		Mathematics = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 1))), // 1 là ID của môn học Toán
			//		Literature = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 2))), // 2 là ID của môn học Văn học
			//		Physics = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 3))), // 3 là ID của môn học Vật lý
			//		Chemistry = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 4))), // 4 là ID của môn học Hóa học
			//		Biology = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 5))), // 5 là ID của môn học Sinh học
			//		CombinedNaturalSciences = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 6))), // 6 là ID của môn học Khoa học tự nhiên tổ hợp
			//		History = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 7))), // 7 là ID của môn học Lịch sử
			//		Geography = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 8))), // 8 là ID của môn học Địa lý
			//		CivicEducation = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 9))), // 9 là ID của môn học Giáo dục công dân
			//		CombinedSocialSciences = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 10))), // 10 là ID của môn học Khoa học xã hội tổ hợp
			//		English = group.Sum(schoolYear => students.Count(s =>
			//			s.SchoolYearId == schoolYear.Id &&
			//			scores.Any(score => score.StudentId == s.Id && score.SubjectId == 11))) // 11 là ID của môn học Tiếng Anh
			//	})
			//	.ToList();

			//listView.ItemsSource = listViewItems;
		}

		private void btnShow5Stu_Click(object sender, RoutedEventArgs e)
		{
			int year;
			string selectedValue = comboBox.Text;

			if (int.TryParse(selectedValue, out year))
			{
				var students = db.Students.Where(s => s.ExamYear == year).ToList();

				if (students.Any())
				{
					List<TopStudent> topStudents = new List<TopStudent>();

					//var topStudentA = students.OrderByDescending(s => s.Mathematics + s.Physics + s.Chemistry).FirstOrDefault();
					//if (topStudentA != null)
					//{
					//	topStudents.Add(new TopStudent
					//	{
					//		KhoiThi = "A",
					//		StudentCode = topStudentA.StudentCode,
					//		DiemMon1 = (double)topStudentA.Mathematics,
					//		DiemMon2 = (double)topStudentA.Physics,
					//		DiemMon3 = (double)topStudentA.Chemistry,
					//		TongDiem = (double)(topStudentA.Mathematics + topStudentA.Physics + topStudentA.Chemistry),
					//		TenMon1 = "Toán",
					//		TenMon2 = "Lý",
					//		TenMon3 = "Hóa"
					//	});
					//}

					double maxTotalScoreA = (double)students.Max(s => s.Mathematics + s.Physics + s.Chemistry);
					var topStudentsA = students.Where(s => s.Mathematics + s.Physics + s.Chemistry == maxTotalScoreA).ToList();
					foreach (var student in topStudentsA)
					{
						topStudents.Add(new TopStudent
						{
							KhoiThi = "A",
							StudentCode = student.StudentCode,
							DiemMon1 = (double)student.Mathematics,
							DiemMon2 = (double)student.Physics,
							DiemMon3 = (double)student.Chemistry,
							TongDiem = (double)(student.Mathematics + student.Physics + student.Chemistry),
							TenMon1 = "Toán",
							TenMon2 = "Lý",
							TenMon3 = "Hóa"
						});
					}

					//var topStudentB = students.OrderByDescending(s => s.Mathematics + s.Chemistry + s.Biology).FirstOrDefault();
					//if (topStudentB != null)
					//{
					//	topStudents.Add(new TopStudent
					//	{
					//		KhoiThi = "B",
					//		StudentCode = topStudentB.StudentCode,
					//		DiemMon1 = (double)topStudentB.Mathematics,
					//		DiemMon2 = (double)topStudentB.Chemistry,
					//		DiemMon3 = (double)topStudentB.Biology,
					//		TongDiem = (double)(topStudentB.Mathematics + topStudentB.Chemistry + topStudentB.Biology),
					//		TenMon1 = "Toán",
					//		TenMon2 = "Hóa",
					//		TenMon3 = "Sinh"
					//	});
					//}

					//var topStudentC = students.OrderByDescending(s => s.Literature + s.History + s.Geography).FirstOrDefault();
					//if (topStudentC != null)
					//{
					//	topStudents.Add(new TopStudent
					//	{
					//		KhoiThi = "C",
					//		StudentCode = topStudentC.StudentCode,
					//		DiemMon1 = (double)topStudentC.Literature,
					//		DiemMon2 = (double)topStudentC.History,
					//		DiemMon3 = (double)topStudentC.Geography,
					//		TongDiem = (double)(topStudentC.Literature + topStudentC.History + topStudentC.Geography),
					//		TenMon1 = "Văn",
					//		TenMon2 = "Sử",
					//		TenMon3 = "Địa"
					//	});
					//}

					//var topStudentD1 = students.OrderByDescending(s => s.Mathematics + s.Literature + s.English).FirstOrDefault();
					//if (topStudentD1 != null)
					//{
					//	topStudents.Add(new TopStudent
					//	{
					//		KhoiThi = "D1",
					//		StudentCode = topStudentD1.StudentCode,
					//		DiemMon1 = (double)topStudentD1.Mathematics,
					//		DiemMon2 = (double)topStudentD1.Literature,
					//		DiemMon3 = (double)topStudentD1.English,
					//		TongDiem = (double)(topStudentD1.Mathematics + topStudentD1.Literature + topStudentD1.English),
					//		TenMon1 = "Toán",
					//		TenMon2 = "Văn",
					//		TenMon3 = "Tiếng Anh"
					//	});
					//}

					//var topStudentA1 = students.OrderByDescending(s => s.Mathematics + s.Physics + s.English).FirstOrDefault();
					//if (topStudentA1 != null)
					//{
					//	topStudents.Add(new TopStudent
					//	{
					//		KhoiThi = "A1",
					//		StudentCode = topStudentA1.StudentCode,
					//		DiemMon1 = (double)topStudentA1.Mathematics,
					//		DiemMon2 = (double)topStudentA1.Physics,
					//		DiemMon3 = (double)topStudentA1.English,
					//		TongDiem = (double)(topStudentA1.Mathematics + topStudentA1.Physics + topStudentA1.English),
					//		TenMon1 = "Toán",
					//		TenMon2 = "Lý",
					//		TenMon3 = "Tiếng Anh"
					//	});
					//}

					double maxTotalScoreB = (double)students.Max(s => s.Mathematics + s.Chemistry + s.Biology);
					var topStudentsB = students.Where(s => s.Mathematics + s.Chemistry + s.Biology == maxTotalScoreB).ToList();
					foreach (var student in topStudentsB)
					{
						topStudents.Add(new TopStudent
						{
							KhoiThi = "B",
							StudentCode = student.StudentCode,
							DiemMon1 = (double)student.Mathematics,
							DiemMon2 = (double)student.Chemistry,
							DiemMon3 = (double)student.Biology,
							TongDiem = (double)(student.Mathematics + student.Chemistry + student.Biology),
							TenMon1 = "Toán",
							TenMon2 = "Hóa",
							TenMon3 = "Sinh"
						});
					}

					double maxTotalScoreC = (double)students.Max(s => s.Literature + s.History + s.Geography);
					var topStudentsC = students.Where(s => s.Literature + s.History + s.Geography == maxTotalScoreC).ToList();
					foreach (var student in topStudentsC)
					{
						topStudents.Add(new TopStudent
						{
							KhoiThi = "C",
							StudentCode = student.StudentCode,
							DiemMon1 = (double)student.Literature,
							DiemMon2 = (double)student.History,
							DiemMon3 = (double)student.Geography,
							TongDiem = (double)(student.Literature + student.History + student.Geography),
							TenMon1 = "Văn",
							TenMon2 = "Sử",
							TenMon3 = "Địa"
						});
					}

					double maxTotalScoreD1 = (double)students.Max(s => s.Mathematics + s.Literature + s.English);
					var topStudentsD1 = students.Where(s => s.Mathematics + s.Literature + s.English == maxTotalScoreD1).ToList();
					foreach (var student in topStudentsD1)
					{
						topStudents.Add(new TopStudent
						{
							KhoiThi = "D1",
							StudentCode = student.StudentCode,
							DiemMon1 = (double)student.Mathematics,
							DiemMon2 = (double)student.Literature,
							DiemMon3 = (double)student.English,
							TongDiem = (double)(student.Mathematics + student.Literature + student.English),
							TenMon1 = "Toán",
							TenMon2 = "Văn",
							TenMon3 = "Tiếng Anh"
						});
					}

					double maxTotalScoreA1 = (double)students.Max(s => s.Mathematics + s.Physics + s.English);
					var topStudentsA1 = students.Where(s => s.Mathematics + s.Physics + s.English == maxTotalScoreA1).ToList();
					foreach (var student in topStudentsA1)
					{
						topStudents.Add(new TopStudent
						{
							KhoiThi = "A1",
							StudentCode = student.StudentCode,
							DiemMon1 = (double)student.Mathematics,
							DiemMon2 = (double)student.Physics,
							DiemMon3 = (double)student.English,
							TongDiem = (double)(student.Mathematics + student.Physics + student.English),
							TenMon1 = "Toán",
							TenMon2 = "Lý",
							TenMon3 = "Tiếng Anh"
						});
					}

					listView5Stu.ItemsSource = topStudents;
				}
				else
				{
					WinForms.MessageBox.Show("Không tìm thấy năm học.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}	
			}
			else
			{
				WinForms.MessageBox.Show("Vui lòng chọn năm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		//private void btnImport_Click(object sender, EventArgs e)
		//{
		//	string filePath = txtFileName.Text;
		//	int year;
		//	string selectedValue = comboBox.Text;

		//	if (int.TryParse(selectedValue, out year) && !string.IsNullOrEmpty(filePath))
		//	{
		//		var studentsQueue = new Queue<StudentScore>(); // Hàng đợi lưu trữ tạm thời các học sinh

		//		using (var reader = new StreamReader(filePath))
		//		{
		//			reader.ReadLine(); // Bỏ qua dòng tiêu đề

		//			while (!reader.EndOfStream)
		//			{
		//				var line = reader.ReadLine();
		//				var values = line.Split(',');

		//				var student = new StudentScore
		//				{
		//					StudentId = values[0],
		//					Province = values[1],
		//					Scores = new List<SubjectScore>()
		//				};

		//				for (int i = 2; i < values.Length; i++)
		//				{
		//					int subjectId = GetSubjectNameByIndex(i); // Hàm để lấy tên môn học tương ứng với chỉ số cột

		//					if (!string.IsNullOrEmpty(values[i]))
		//					{
		//						double scoreValue = double.Parse(values[i]);
		//						student.Scores.Add(new SubjectScore { SubjectId = subjectId, Score = scoreValue });
		//					}
		//				}

		//				studentsQueue.Enqueue(student); // Thêm học sinh vào hàng đợi
		//			}
		//		}

		//		try
		//		{
		//			using (var transaction = db.Database.BeginTransaction())
		//			{
		//				var check = db.SchoolYears.FirstOrDefault(s => s.ExamYear == year);

		//				if (check == null)
		//				{
		//					var provincesName = studentsQueue.Select(s => s.Province).Distinct();
		//					var schoolYears = new List<SchoolYear>();
		//					var students = new List<Student>();
		//					var scores = new List<Score>();

		//					foreach (var provinceName in provincesName)
		//					{
		//						var schoolYear = new SchoolYear()
		//						{
		//							Name = provinceName,
		//							ExamYear = year,
		//							Status = "True"
		//						};
		//						schoolYears.Add(schoolYear);
		//					}

		//					db.SchoolYears.AddRange(schoolYears);
		//					db.SaveChanges();

		//					foreach (var schoolYear in schoolYears)
		//					{
		//						var schoolYearId = schoolYear.Id;
		//						var studentsList = new List<StudentScore>(studentsQueue); // Tạo collection tạm thời từ hàng đợi

		//						while (studentsList.Count > 0)
		//						{
		//							var student = studentsList[0]; // Lấy học sinh từ collection tạm thời
		//							studentsList.RemoveAt(0); // Xóa học sinh từ collection tạm thời

		//							var newStudent = new Student
		//							{
		//								StudentCode = student.StudentId,
		//								SchoolYearId = schoolYearId,
		//								Status = "True"
		//							};
		//							db.Students.Add(newStudent);
		//						}
		//					}

		//					db.SaveChanges();
		//					transaction.Commit();
		//				}
		//				else
		//				{
		//					WinForms.MessageBox.Show("File đã được import, vui lòng chọn file khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//				}
		//			}
		//		}
		//		catch (Exception ex)
		//		{
		//			// Handle the exception appropriately (e.g., log the error)
		//			WinForms.MessageBox.Show("Đã xảy ra lỗi khi thêm dữ liệu. Vui lòng thử lại sau.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);

		//		}
		//	}
		//	else if (string.IsNullOrEmpty(filePath))
		//	{
		//		WinForms.MessageBox.Show("Vui lòng chọn file để import.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//	}
		//	else
		//	{
		//		WinForms.MessageBox.Show("Vui lòng chọn năm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//	}
		//}


		//private void btnImport_Click(object sender, RoutedEventArgs e)
		//{
		//	string filePath = txtFileName.Text;
		//	int year;
		//	string selectedValue = comboBox.Text;

		//	if (int.TryParse(selectedValue, out year) && !filePath.IsNullOrEmpty())
		//	{
		//		var students = ReadFromFileCsv(filePath);

		//		var check = db.SchoolYears.Where(s => s.ExamYear == year).FirstOrDefault();

		//		if (check is null)
		//		{
		//			var provincesName = students.Select(s => s.Province).Distinct();

		//			Queue<StudentScore> studentQueue = new Queue<StudentScore>(students);

		//			foreach (var provinceName in provincesName)
		//			{
		//				var schoolYear = new SchoolYear()
		//				{
		//					Name = provinceName,
		//					ExamYear = year,
		//					Status = "True"
		//				};
		//				db.SchoolYears.Add(schoolYear);
		//				db.SaveChanges();

		//				var schoolYearId = schoolYear.Id;

		//				while (studentQueue.Count > 0)
		//				{
		//					var student = studentQueue.Dequeue();
		//					var newStudent = new Student
		//					{
		//						StudentCode = student.StudentId,
		//						SchoolYearId = schoolYearId,
		//						Status = "True"
		//					};
		//					db.Students.Add(newStudent);
		//					db.SaveChanges();

		//					var studentId = newStudent.Id;

		//					foreach (var score in student.Scores)
		//					{
		//						var newScore = new Score
		//						{
		//							StudentId = studentId,
		//							SubjectId = score.SubjectId,
		//							Score1 = score.Score
		//						};
		//						db.Scores.Add(newScore);
		//					}
		//					db.SaveChanges();
		//				}
		//			}
		//		}
		//		else
		//		{
		//			WinForms.MessageBox.Show("File đã được import, vui lòng chọn file khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//		}
		//	}
		//	else if (filePath.IsNullOrEmpty())
		//	{
		//		WinForms.MessageBox.Show("Vui lòng chọn file để import.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//	}
		//	else
		//	{
		//		WinForms.MessageBox.Show("Vui lòng chọn năm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//	}
		//}
	}


	public class SubjectScore
	{
		public int SubjectId { get; set; }
		public double Score { get; set; }
	}

	public class StudentScore
	{
		public string StudentId { get; set; }
		public string Province { get; set; }
		public List<SubjectScore> Scores { get; set; }

		public StudentScore()
		{
			Scores = new List<SubjectScore>();
		}
	}

	/* không dùng 2 thằng trên thì dùng thằng này (dictionary) */

	//public class StudentScore
	//{
	//	public string StudentId { get; set; }
	//	public string Province { get; set; }
	//	public Dictionary<int, double> Scores { get; set; }

	//	public StudentScore()
	//	{
	//		Scores = new Dictionary<int, double>();
	//	}
	//}

	public class StudentListEachYear
	{
		public int Year { get; set; }

		public double TotalStudents { get; set; }

		public double Mathematics { get; set; }

		public double Literature { get; set; }

		public double Physics { get; set; }

		public double Chemistry { get; set; }

		public double Biology { get; set; }

		public double CombinedNaturalSciences { get; set; }

		public double History { get; set; }

		public double Geography { get; set; }

		public double CivicEducation { get; set; }

		public double CombinedSocialSciences { get; set; }

		public double English { get; set; }
	}

	public class Students
	{
		public string StudentId { get; set; }

		public string Province { get; set; }

		public string Mathematics { get; set; }
		
		public string Literature { get; set; }

		public string Physics { get; set; }

		public string Chemistry { get; set; }

		public string Biology { get; set; }

		public string CombinedNaturalSciences { get; set; }

		public string History { get; set; }

		public string Geography { get; set; }

		public string CivicEducation { get; set; }

		public string CombinedSocialSciences { get; set; }

		public string English { get; set; }
	}

	public class TopStudent
	{
		public string KhoiThi { get; set; }
		public string StudentCode { get; set; }
		public double DiemMon1 { get; set; }
		public double DiemMon2 { get; set; }
		public double DiemMon3 { get; set; }
		public double TongDiem { get; set; }
		public string TenMon1 { get; set; }
		public string TenMon2 { get; set; }
		public string TenMon3 { get; set; }
		public string TenMon
		{
			get { return TenMon1 + ", " + TenMon2 + ", " + TenMon3; }
		}
	}
}
