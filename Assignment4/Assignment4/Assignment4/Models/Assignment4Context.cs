﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Assignment4.Models;

public partial class Assignment4Context : DbContext
{
    public Assignment4Context()
    {
    }

    public Assignment4Context(DbContextOptions<Assignment4Context> options)
        : base(options)
    {
    }

    public virtual DbSet<Student> Students { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=(local);uid=sa;pwd=12345;database=Assignment4;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Student>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Student__3214EC075551D9FE");

            entity.ToTable("Student");

            entity.Property(e => e.CivicEducation).HasColumnName("Civic_Education");
            entity.Property(e => e.CombinedNaturalSciences).HasColumnName("Combined_Natural_Sciences");
            entity.Property(e => e.CombinedSocialSciences).HasColumnName("Combined_Social_Sciences");
            entity.Property(e => e.Province)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.StudentCode)
                .HasMaxLength(20)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
