﻿-- Create database
USE MASTER
GO
DROP DATABASE IF EXISTS Assignment4
GO
CREATE DATABASE Assignment4
GO

USE Assignment4;
GO

-- Tạo bảng Student
CREATE TABLE Student (
	Id INT IDENTITY(1,1) PRIMARY KEY,
    StudentCode VARCHAR(20),
	ExamYear INT,
    Province NVARCHAR(100),
    Mathematics FLOAT,
    Literature FLOAT,
    Physics FLOAT,
    Chemistry FLOAT,
    Biology FLOAT,
    Combined_Natural_Sciences FLOAT,
    History FLOAT,
    Geography FLOAT,
    Civic_Education FLOAT,
    Combined_Social_Sciences FLOAT,
    English FLOAT
);


---- Tạo bảng SchoolYear
--CREATE TABLE SchoolYear (
--    Id INT IDENTITY(1,1) PRIMARY KEY,
--    Name NVARCHAR(100),
--    ExamYear INT,
--    Status VARCHAR(50)
--);

---- Tạo bảng Subject
--CREATE TABLE Subject (
--    Id INT IDENTITY(1,1) PRIMARY KEY,
--    Code VARCHAR(20),
--    Name VARCHAR(100)
--);

---- Tạo bảng Student
--CREATE TABLE Student (
--    Id INT IDENTITY(1,1) PRIMARY KEY,
--    StudentCode VARCHAR(20),
--    SchoolYearId INT,
--    Status VARCHAR(50),
--    --FOREIGN KEY (SchoolYearId) REFERENCES SchoolYear(Id)
--);

---- Tạo bảng Score
--CREATE TABLE Score (
--    Id INT IDENTITY(1,1) PRIMARY KEY,
--    StudentId INT,
--    SubjectId INT,
--    Score FLOAT,
--    --FOREIGN KEY (StudentId) REFERENCES Student(Id),
--    --FOREIGN KEY (SubjectId) REFERENCES Subject(Id)
--);

--INSERT INTO Subject VALUES ('math', 'Mathematics');
--INSERT INTO Subject VALUES ('literature', 'Literature');
--INSERT INTO Subject VALUES ('physics', 'Physics');
--INSERT INTO Subject VALUES ('chemistry', 'Chemistry');
--INSERT INTO Subject VALUES ('biology', 'Biology');
--INSERT INTO Subject VALUES ('natural_sciences', 'Combined Natural Sciences');
--INSERT INTO Subject VALUES ('history', 'History');
--INSERT INTO Subject VALUES ('geography', 'Geography');
--INSERT INTO Subject VALUES ('civic_education', 'Civic Education');
--INSERT INTO Subject VALUES ('social_sciences', 'Combined Socia Sciences');
--INSERT INTO Subject VALUES ('english', 'English');