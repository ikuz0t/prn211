﻿using Microsoft.EntityFrameworkCore;

namespace Lab3.Models
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext()
        {
        }

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }

        public virtual DbSet<Products> Products { get; set; }
    }
}
