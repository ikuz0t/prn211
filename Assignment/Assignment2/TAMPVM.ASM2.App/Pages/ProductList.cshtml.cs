﻿using Assignment2.Pages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TAMPVM.ASM2.Repo.Entities;
using TAMPVM.ASM2.Repo.Repositories;

namespace TAMPVM.ASM2.App.Pages
{
    public class ProductListModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public ProductListModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Product> Products { get; set; }

        [BindProperty(SupportsGet = true)] //  cho phép thuộc tính SearchString được gắn kết với dữ liệu được truyền vào từ yêu cầu HTTP theo phương thức GET
        public string SearchString { get; set; }


        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages => (int)Math.Ceiling((double)TotalItems / PageSize);

        public void OnGet(int? pageIndex, int? pageSize)
        {
            PageIndex = pageIndex ?? 1; // Sử dụng giá trị trang mặc định là 1 nếu không có giá trị được truyền vào
            PageSize = pageSize ?? 10; // Sử dụng giá trị kích thước trang mặc định là 10 nếu không có giá trị được truyền vào

            Products = _unitOfWork.ProductRepository.Get(
                filter: p => string.IsNullOrEmpty(SearchString) || p.ProductName.Contains(SearchString),
                orderBy: q => q.OrderBy(p => p.ProductId),
                pageIndex: PageIndex,
                pageSize: PageSize).ToList();

            TotalItems = _unitOfWork.ProductRepository.Get(
                filter: p => string.IsNullOrEmpty(SearchString) || p.ProductName.Contains(SearchString))
                .Count();
        }
    }

}
