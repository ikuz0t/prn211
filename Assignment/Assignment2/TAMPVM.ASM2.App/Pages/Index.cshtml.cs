﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Assignment2.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public IActionResult OnGet()
        {
            var user = HttpContext.Session.GetString("CurrentUser");

            if (string.IsNullOrEmpty(user))
            {
                return RedirectToPage("/Login");
            }
            return Page();
        }
    }
}