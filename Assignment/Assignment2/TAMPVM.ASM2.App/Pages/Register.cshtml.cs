﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using TAMPVM.ASM2.Repo.Entities;
using TAMPVM.ASM2.Repo.Repositories;

namespace TAMPVM.ASM2.App.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public RegisterModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập tên người dùng.")]
        public string Username { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu.")]
        public string Password { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập họ và tên.")]
        public string FullName { get; set; }

        //public IActionResult OnGet()
        //{
        //    var user = HttpContext.Session.GetString("CurrentUser");

        //    if (user != null)
        //    {
        //        return RedirectToPage("/Index");
        //    }

        //    return Page();
        //}

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                // DTO không hợp lệ, có lỗi xảy ra
                return Page();
            }

            var check = _unitOfWork.AccountRepository.Get(u => u.UserName == Username).FirstOrDefault();

            if (check != null)
            {
                // Đăng ký không thành công, trở lại trang đăng ký với thông báo lỗi
                ViewData["ErrorMessage"] = "Đăng ký không thành công. Người dùng đã tồn tại.";
                return Page();
            }
            else
            {
                var newUser = new Account
                {
                    UserName = Username,
                    Password = Password,
                    FullName = FullName,
                    Type = "0"
                };

                _unitOfWork.AccountRepository.Insert(newUser);
                _unitOfWork.SaveChange();

                return RedirectToPage("/Login");
            }
        }


    }
}
