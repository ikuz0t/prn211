﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using TAMPVM.ASM2.Repo.Entities;
using TAMPVM.ASM2.Repo.Repositories;

namespace TAMPVM.ASM2.App.Pages
{
    public class UpdateProductModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public UpdateProductModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập tên sản phẩm.")]
        public string ProductName { get; set; }

        //[BindProperty]
        //[Required(ErrorMessage = "Vui lòng chọn nhà cung cấp.")]
        //public int? SupplierId { get; set; }

        //[BindProperty]
        //[Required(ErrorMessage = "Vui lòng chọn danh mục.")]
        //public int? CategoryId { get; set; }

        //[BindProperty]
        //[Required(ErrorMessage = "Vui lòng nhập số lượng cho mỗi đơn vị.")]
        //public string? QuantityPerUnit { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Vui lòng nhập giá sản phẩm.")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Giá sản phẩm phải lớn hơn 0.")]
        public decimal UnitPrice { get; set; }

        //[BindProperty]
        //[Required(ErrorMessage = "Vui lòng nhập giá sản phẩm.")]
        //public byte[]? ProductImage { get; set; }

        public void OnGet()
        {
        }

        public IActionResult OnPost(int productId)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var product = _unitOfWork.ProductRepository.GetByID(productId);

            product.ProductName = ProductName;
            product.UnitPrice = UnitPrice;

            _unitOfWork.ProductRepository.Update(product);
            _unitOfWork.SaveChange();

            // Chuyển hướng đến trang danh sách sản phẩm
            return RedirectToPage("/ProductList");
        }
    }
}
