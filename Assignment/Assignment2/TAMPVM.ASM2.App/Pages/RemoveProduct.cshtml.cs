using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TAMPVM.ASM2.Repo.Entities;
using TAMPVM.ASM2.Repo.Repositories;

namespace TAMPVM.ASM2.App.Pages
{
    public class RemoveProductModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public RemoveProductModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult OnGet(int productId)
        {
            var product = _unitOfWork.ProductRepository.GetByID(productId);

            _unitOfWork.ProductRepository.Delete(product);
            _unitOfWork.SaveChange();

            return RedirectToPage("/ProductList");
        }

        //public IActionResult OnPost(int productId)
        //{
        //    var product = _unitOfWork.ProductRepository.GetByID(productId);

        //    _unitOfWork.ProductRepository.Delete(product);
        //    _unitOfWork.SaveChange();

        //    return RedirectToPage("/ProductList");
        //}

    }
}
