using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace TAMPVM.ASM2.App.Pages.Shared
{
    public class LogoutModel : PageModel
    {
        public IActionResult OnGet()
        {
            HttpContext.Session.Remove("CurrentUser");
            HttpContext.Session.Remove("Type");

            return RedirectToPage("/Index");
        }
    }
}
