﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAMPVM.ASM2.Repo.Entities;

namespace TAMPVM.ASM2.Repo.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private readonly Assignment2Context _dbContext;
        private GenericRepository<Account> _accountRepository;
        private GenericRepository<Product> _productRepository;

        public UnitOfWork(Assignment2Context dbContext
        )
        {
            _dbContext = dbContext;
        }

        public GenericRepository<Account> AccountRepository
        {
            get
            {
                if (_accountRepository == null)
                {
                    _accountRepository = new GenericRepository<Account>(_dbContext);
                }
                return _accountRepository;
            }
        }

        public GenericRepository<Product> ProductRepository
        {
            get
            {
                if (_productRepository == null)
                {
                    _productRepository = new GenericRepository<Product>(_dbContext);
                }
                return _productRepository;
            }
        }

        public int SaveChange()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
