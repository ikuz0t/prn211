﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Repository.Models;
using Repository.Repository;

namespace PE_PRN.Pages
{
    public class IndexModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public IndexModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Eyeglass> Eyeglasses { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages => (int)Math.Ceiling((double)TotalItems / PageSize);

        public string Sort { get; set; }


        public IActionResult OnGet(int? pageIndex)
        {
            var user = HttpContext.Session.GetString("CurrentUser");

            if (string.IsNullOrEmpty(user))
            {
                return RedirectToPage("/Login");
            }

            PageIndex = pageIndex ?? 1; // Sử dụng giá trị trang mặc định là 1 nếu không có giá trị được truyền vào
            PageSize = 4; // Sử dụng giá trị kích thước trang mặc định là 10 nếu không có giá trị được truyền vào


            if (Sort == "ascending")
            {
                var eyeglassesQuery = _unitOfWork.EyeglassRepository.Get(
                orderBy: q => q.OrderBy(e => e.Price),
                includeProperties: "LensType",
                pageIndex: PageIndex,
                pageSize: PageSize
                );

                TotalItems = _unitOfWork.EyeglassRepository.Count();

                Eyeglasses = eyeglassesQuery.ToList();
            }
            else if (Sort == "descending")
            {
                var eyeglassesQuery = _unitOfWork.EyeglassRepository.Get(
                orderBy: q => q.OrderByDescending(e => e.Price),
                includeProperties: "LensType",
                pageIndex: PageIndex,
                pageSize: PageSize
                );

                TotalItems = _unitOfWork.EyeglassRepository.Count();

                Eyeglasses = eyeglassesQuery.ToList();
            }
            else
            {
                var eyeglassesQuery = _unitOfWork.EyeglassRepository.Get(
                orderBy: q => q.OrderByDescending(e => e.CreatedDate).ThenBy(e => e.EyeglassesId),
                includeProperties: "LensType",
                pageIndex: PageIndex,
                pageSize: PageSize
                );

                TotalItems = _unitOfWork.EyeglassRepository.Count();

                Eyeglasses = eyeglassesQuery.ToList();
            }

            return Page();
        }

        public IActionResult OnPost(int? pageIndex, int? pageSize)
        {
            var user = HttpContext.Session.GetString("CurrentUser");

            if (string.IsNullOrEmpty(user))
            {
                return RedirectToPage("/Login");
            }

            PageIndex = pageIndex ?? 1; // Sử dụng giá trị trang mặc định là 1 nếu không có giá trị được truyền vào
            PageSize = pageSize ?? 4; // Sử dụng giá trị kích thước trang mặc định là 10 nếu không có giá trị được truyền vào

            Sort = Request.Form["sortOrder"];



            if (Sort == "ascending")
            {
                var eyeglassesQuery = _unitOfWork.EyeglassRepository.Get(
                orderBy: q => q.OrderBy(e => e.Price),
                includeProperties: "LensType",
                pageIndex: PageIndex,
                pageSize: PageSize
                );

                TotalItems = _unitOfWork.EyeglassRepository.Count();

                Eyeglasses = eyeglassesQuery.ToList();
            }
            else if (Sort == "descending")
            {
                var eyeglassesQuery = _unitOfWork.EyeglassRepository.Get(
                orderBy: q => q.OrderByDescending(e => e.Price),
                includeProperties: "LensType",
                pageIndex: PageIndex,
                pageSize: PageSize
                );

                TotalItems = _unitOfWork.EyeglassRepository.Count();

                Eyeglasses = eyeglassesQuery.ToList();
            }
            else
            {
                var eyeglassesQuery = _unitOfWork.EyeglassRepository.Get(
                orderBy: q => q.OrderByDescending(e => e.CreatedDate).ThenBy(e => e.EyeglassesId),
                includeProperties: "LensType",
                pageIndex: PageIndex,
                pageSize: PageSize
                );

                TotalItems = _unitOfWork.EyeglassRepository.Count();

                Eyeglasses = eyeglassesQuery.ToList();
            }



            return Page();
        }
    }
}