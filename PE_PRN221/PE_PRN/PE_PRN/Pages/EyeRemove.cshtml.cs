using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Repository.Repository;

namespace PE_PRN.Pages
{
    public class EyeRemoveModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public EyeRemoveModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult OnGet(int EyeglassesId)
        {
            var eyeglass = _unitOfWork.EyeglassRepository.GetByID(EyeglassesId);

            _unitOfWork.EyeglassRepository.Delete(eyeglass);
            _unitOfWork.SaveChange();

            return RedirectToPage("/Index");
        }
    }
}
