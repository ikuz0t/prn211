﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Repository.Models;
using Repository.Repository;

namespace PE_PRN.Pages
{
    public class EyeListModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public EyeListModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Eyeglass> Eyeglasses { get; set; }

        [BindProperty(SupportsGet = true)] //  cho phép thuộc tính SearchString được gắn kết với dữ liệu được truyền vào từ yêu cầu HTTP theo phương thức GET
        public string SearchString { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages => (int)Math.Ceiling((double)TotalItems / PageSize);

        public void OnGet(int? pageIndex, string searchString)
        {
            PageIndex = pageIndex ?? 1; // Sử dụng giá trị trang mặc định là 1 nếu không có giá trị được truyền vào
            PageSize = 4; // Sử dụng giá trị kích thước trang mặc định là 10 nếu không có giá trị được truyền vào
            SearchString = searchString;

            var eyeglassesQuery = _unitOfWork.EyeglassRepository.Get(
                    filter: p => p.Price.ToString().Contains(SearchString)  || p.EyeglassesDescription.Contains(SearchString),
                    orderBy: q => q.OrderByDescending(e => e.Price).ThenBy(e => e.EyeglassesId),
                    includeProperties: "LensType",
                    pageIndex: PageIndex,
                    pageSize: PageSize
                    );

                TotalItems = _unitOfWork.EyeglassRepository.Count(filter: p => p.Price.ToString().Contains(SearchString) || p.EyeglassesDescription.Contains(SearchString));

                Eyeglasses = eyeglassesQuery.ToList();


            }


        }
    
}
