﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Repository.Models;
using Repository.Repository;

namespace PE_PRN.Pages
{
    public class EyeUpdateModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public EyeUpdateModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [BindProperty] // liên kết dữ liệu giữa các trường dữ liệu của trang Razor và thuộc tính của model
        public Eyeglass Eyeglass { get; set; }

        public SelectList LensTypeNameOptions { get; set; }

        public IActionResult OnGet(int EyeglassesId) //truyền đúng tên field ở view Index
        {
            Eyeglass = _unitOfWork.EyeglassRepository.GetByID(EyeglassesId);

            if (Eyeglass == null)
            {
                return NotFound();
            }

            // Lấy danh sách LensType từ UnitOfWork
            var lensTypes = _unitOfWork.LensTypeRepository.Get();

            // Tạo SelectList từ danh sách LensType và chọn LensTypeName tương ứng với Eyeglass
            LensTypeNameOptions = new SelectList(lensTypes, "LensTypeId", "LensTypeName", Eyeglass.LensTypeId);

            return Page();
        }

        public IActionResult OnPost()
        {
            // Lấy danh sách LensType từ UnitOfWork
            var lensTypes = _unitOfWork.LensTypeRepository.Get();

            // Tạo SelectList từ danh sách LensType và chọn LensTypeName tương ứng với Eyeglass
            LensTypeNameOptions = new SelectList(lensTypes, "LensTypeId", "LensTypeName", Eyeglass.LensTypeId);

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var eyeglass = _unitOfWork.EyeglassRepository.GetByID(Eyeglass.EyeglassesId);

            if (eyeglass == null)
            {
                return NotFound();
            }

            eyeglass.EyeglassesName = Eyeglass.EyeglassesName;
            eyeglass.EyeglassesDescription = Eyeglass.EyeglassesDescription;
            eyeglass.FrameColor = Eyeglass.FrameColor;
            eyeglass.Price = Eyeglass.Price;
            eyeglass.Quantity = Eyeglass.Quantity;
            eyeglass.CreatedDate = DateTime.Now;
            eyeglass.LensTypeId = Eyeglass.LensTypeId;

            _unitOfWork.EyeglassRepository.Update(eyeglass);
            _unitOfWork.SaveChange();

            return RedirectToPage("/Index");
        }
    }
}
