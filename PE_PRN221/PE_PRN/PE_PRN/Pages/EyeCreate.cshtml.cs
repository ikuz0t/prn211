﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Repository.Models;
using Repository.Repository;

namespace PE_PRN.Pages
{
    public class EyeCreateModel : PageModel
    {
        private readonly UnitOfWork _unitOfWork;

        public EyeCreateModel(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [BindProperty] // liên kết dữ liệu giữa các trường dữ liệu của trang Razor và thuộc tính của model
        public Eyeglass Eyeglass { get; set; }

        public SelectList LensTypeNameOptions { get; set; }

        public void OnGet()
        {
            // Lấy danh sách LensType từ UnitOfWork
            var lensTypes = _unitOfWork.LensTypeRepository.Get();

            // Tạo SelectList từ danh sách LensType và chọn LensTypeName tương ứng với Eyeglass
            LensTypeNameOptions = new SelectList(lensTypes, "LensTypeId", "LensTypeName");
        }

        public IActionResult OnPost()
        {
            // Lấy danh sách LensType từ UnitOfWork
            var lensTypes = _unitOfWork.LensTypeRepository.Get();

            // Tạo SelectList từ danh sách LensType và chọn LensTypeName tương ứng với Eyeglass
            LensTypeNameOptions = new SelectList(lensTypes, "LensTypeId", "LensTypeName");

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var existWyeglass = _unitOfWork.EyeglassRepository.GetByID(Eyeglass.EyeglassesId);

            if (existWyeglass != null)
            {
                ViewData["ErrorMessage"] = "Exist Id";
                return Page();
            }
            else
            {
                var eyeglass = new Eyeglass
                {
                    EyeglassesId = Eyeglass.EyeglassesId,
                    EyeglassesName = Eyeglass.EyeglassesName,
                    EyeglassesDescription = Eyeglass.EyeglassesDescription,
                    FrameColor = Eyeglass.FrameColor,
                    Price = Eyeglass.Price,
                    Quantity = Eyeglass.Quantity,
                    CreatedDate = DateTime.Now,
                    LensTypeId = Eyeglass.LensTypeId
                };


                _unitOfWork.EyeglassRepository.Insert(eyeglass);
                _unitOfWork.SaveChange();
            }

            

            return RedirectToPage("/Index");
        }
    }
}
