﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repository
{
    public class UnitOfWork : IDisposable
    {
        private readonly Eyeglasses2024DBContext _dbContext;
        private GenericRepository<StoreAccount> _accountRepository;
        private GenericRepository<Eyeglass> _eyeglassRepository;
        private GenericRepository<LensType> _lensTypeRepository;

        public UnitOfWork(Eyeglasses2024DBContext dbContext
        )
        {
            _dbContext = dbContext;
        }

        public GenericRepository<StoreAccount> AccountRepository
        {
            get
            {
                if (_accountRepository == null)
                {
                    _accountRepository = new GenericRepository<StoreAccount>(_dbContext);
                }
                return _accountRepository;
            }
        }

        public GenericRepository<Eyeglass> EyeglassRepository
        {
            get
            {
                if (_eyeglassRepository == null)
                {
                    _eyeglassRepository = new GenericRepository<Eyeglass>(_dbContext);
                }
                return _eyeglassRepository;
            }
        }

        public GenericRepository<LensType> LensTypeRepository
        {
            get
            {
                if (_lensTypeRepository == null)
                {
                    _lensTypeRepository = new GenericRepository<LensType>(_dbContext);
                }
                return _lensTypeRepository;
            }
        }

        public int SaveChange()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
