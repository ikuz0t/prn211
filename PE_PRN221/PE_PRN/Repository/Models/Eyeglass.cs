﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Repository.Models
{
    public partial class Eyeglass
    {
        [Required(ErrorMessage = "Vui lòng nhập Id.")]
        public int EyeglassesId { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên kính.")]
        [StringLength(100, MinimumLength = 11, ErrorMessage = "Tên kính phải có lớn hơn 10 ký tự.")]
        [RegularExpression(@"^([A-Z][A-Za-z0-9@#$&() -]*)$", ErrorMessage = "Tên kính phải bắt đầu bằng chữ cái viết hoa.")]
        [CustomValidation(typeof(Eyeglass), "ValidateEyeglassesName", ErrorMessage = "Mỗi từ trong tên kính phải bắt đầu bằng chữ cái viết hoa.")]
        public string EyeglassesName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mô tả kính.")]
        public string EyeglassesDescription { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập màu gọng.")]
        public string FrameColor { get; set; }

        [Range(1, double.MaxValue, ErrorMessage = "Giá trị của giá phải lớn hơn 0.")]
        public decimal Price { get; set; }

        [Range(0, 999, ErrorMessage = "Giá trị của số lượng phải nằm trong khoảng từ 0 đến 999.")]
        public int Quantity { get; set; }

        public DateTime? CreatedDate { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn loại ống kính.")]
        public string LensTypeId { get; set; }

        public virtual LensType LensType { get; set; }

        public static ValidationResult ValidateEyeglassesName(string eyeglassesName, ValidationContext context)
        {
            if (!string.IsNullOrEmpty(eyeglassesName))
            {
                string[] words = eyeglassesName.Split(' ');
                foreach (string word in words)
                {
                    if (char.IsLower(word[0]))
                    {
                        return new ValidationResult("Mỗi từ trong tên kính phải bắt đầu bằng chữ cái viết hoa.");
                    }
                }
            }
            return ValidationResult.Success;
        }
    }
}
